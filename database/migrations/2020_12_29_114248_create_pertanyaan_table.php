<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('judul');
            $table->string('isi');
            $table->unsignedBigInteger('pertanyaan_has_profil');
            $table->unsignedBigInteger('pertanyaan_has_jawaban');
            $table->foreign('pertanyaan_has_profil')->references('id')->on('profil');
            $table->foreign('pertanyaan_has_jawaban')->references('id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
    }
}
