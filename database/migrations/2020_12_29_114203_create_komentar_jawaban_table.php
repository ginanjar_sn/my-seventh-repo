<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('isi');
            $table->unsignedBigInteger('k_jawaban_has_profil');
            $table->unsignedBigInteger('k_jawaban_has_jawaban');
            $table->foreign('k_jawaban_has_profil')->references('id')->on('profil');
            $table->foreign('k_jawaban_has_jawaban')->references('id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_jawaban');
    }
}
