<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('jawaban');
            $table->string('profil');
            $table->string('poin');
            $table->foreign('id')->references('id')->on('profil');
            $table->foreign('id')->references('id')->on('jawaban');
            $table->unsignedBigInteger('l_d_jawaban_has_profil');
            $table->unsignedBigInteger('l_d_jawaban_has_jawaban');
            $table->foreign('l_d_jawaban_has_profil')->references('id')->on('profil');
            $table->foreign('l_d_jawaban_has_jawaban')->references('id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_jawaban');
    }
}
