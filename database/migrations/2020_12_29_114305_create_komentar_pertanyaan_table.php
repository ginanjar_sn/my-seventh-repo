<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('isi');
            $table->unsignedBigInteger('k_pertanyaan_has_profil');
            $table->unsignedBigInteger('k_pertanyaan_has_pertanyaan');
            $table->foreign('k_pertanyaan_has_profil')->references('id')->on('profil');
            $table->foreign('k_pertanyaan_has_pertanyaan')->references('id')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan');
    }
}
