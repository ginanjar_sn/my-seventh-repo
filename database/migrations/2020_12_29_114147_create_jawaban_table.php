<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('isi');
            $table->unsignedBigInteger('jawaban_has_profil');
            $table->unsignedBigInteger('jawaban_has_pertanyaan');
            $table->foreign('jawaban_has_profil')->references('id')->on('profil');
            $table->foreign('jawaban_has_pertanyaan')->references('id')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban');
    }
}
