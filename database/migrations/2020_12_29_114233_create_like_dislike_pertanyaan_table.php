<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            //editan saya
            $table->string('profil_id')->unique();
            $table->string('poin');
            $table->unsignedBigInteger('l_d_pertanyaan_has_profil');
            $table->unsignedBigInteger('l_d_pertanyaan_has_pertanyaan');
            $table->foreign('l_d_pertanyaan_has_profil')->references('id')->on('profil');
            $table->foreign('l_d_pertanyaan_has_pertanyaan')->references('id')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');
    }
}
